#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>
#include <cstdint>

struct Job {
	int weight;
	int length;

	inline std::istream& read(std::istream &is) {
		is >> weight >> length;
		return is;
	}

	inline std::ostream& write(std::ostream &os) const {
		return os << "[" << weight << "|" << length << "]";
	}

};

inline std::istream& operator>>(std::istream &is, Job &job) {
	return job.read(is);
}

inline std::ostream& operator<<(std::ostream &os, const Job &job) {
	return job.write(os);
}

template<class Compare>
uint64_t completion_sum(const std::vector<Job> jobs, Compare less) {
	std::vector<Job> j(jobs);

	std::stable_sort(j.begin(), j.end(), less);

	uint64_t sum = 0;
	uint64_t completion = 0;
	for(auto i: j) {
		completion += i.length;
		sum += completion*i.weight;
	}

	return sum;	
}

int main(int argc, char **argv) {

	size_t job_count;
	std::cin >> job_count;

	std::vector<Job> jobs(job_count);

	for(size_t i=0;i<job_count;i++) {
		std::cin >> jobs[i];
	}

	auto score1 = [](const Job &a, const Job &b) {
		int diff = (a.weight - a.length) - (b.weight - b.length);
		return diff ? (diff > 0) : (a.weight > b.weight);
	};

	auto score2 = [](const Job &a, const Job &b) {
		float ra = a.weight/(float)a.length;
		float rb = b.weight/(float)b.length;
		return ra > rb;
	};

	std::cout << "Mode 1: " << completion_sum(jobs, score1) << std::endl;	
	std::cout << "Mode 2: " << completion_sum(jobs, score2) << std::endl;	

	return 0;
}