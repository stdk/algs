#include <iostream>
#include <vector>
#include <queue>
#include <algorithm>
#include <graph.h>
#include <weighed_edge.h>
#include <util.h>
#include <csv_locale.h>

class Dijkstra {
	std::vector<bool> marked;
	std::vector<uint32_t> dist;
	std::priority_queue<WeighedEdge, std::vector<WeighedEdge>, 
	                    std::greater<WeighedEdge> > queue;
public:
	Dijkstra(const WeighedDigraph &graph, uint32_t v)
	:marked(graph.V()),dist(graph.V(), std::numeric_limits<uint32_t>::max()) {

		marked[v] = true;
		dist[v] = 0;
		for(auto edge: graph.adj(v)) {
			queue.push(edge);
		}

		while(!queue.empty()) {
			auto edge = queue.top();
			queue.pop();
			if(!marked[edge.v()]) {
				marked[edge.v()] = true;
				dist[edge.v()] = edge.w();
				for(auto next_edge: graph.adj(edge.v())) {
					queue.push(next_edge + edge.w());
				}
			}
		}
	}

	uint32_t distance(uint32_t v) const {
		return dist[v];
	}

};

int main(int argc, char **argv) {
	std::locale::global(std::locale(std::locale(), new csv_locale()));

	WeighedDigraph graph(std::cin);
	if(!graph) {
		std::cerr << "Incorrect graph" << std::endl;
		return 1;
	}

	Dijkstra dijkstra(graph,0);
	
	std::vector<uint32_t> distance = { 7,37,59,82,99,115,133,165,188,197 };
	std::transform(distance.begin(), distance.end(), distance.begin(), [&](uint32_t v) {
		return dijkstra.distance(v-1);
	});

	std::cout << join(",",distance.begin(), distance.end()) << std::endl;

	return 0;
}