#include <iostream>
#include <iterator>
#include <queue>
#include <vector>
#include <cstdint>
#include <functional>
#include <algorithm>


int main() {
	std::istream_iterator<int64_t> input(std::cin), end;

	std::priority_queue<int64_t, std::vector<int64_t>, std::less<int64_t> > left;
	std::priority_queue<int64_t, std::vector<int64_t>, std::greater<int64_t> > right;
	int64_t result = 0;	

	for(;input != end; ++input) {
		if(!left.size() && !right.size()) {
			left.push(*input);
		} else {
			if(*input < left.top()) {
				left.push(*input);
			} else {
				right.push(*input);
			}

			int diff = left.size() - right.size();
			for(int i=diff/2;i>0;i--) {
				right.push(left.top());
				left.pop();
			}

			for(int i=(-diff+1)/2;i>0;i--) {
				left.push(right.top());
				right.pop();
			}
		}

//		std::cout << *input << ": " << (left.size() ? left.top() : -1) << " | " << (right.size() ? right.top() : -1) << std::endl;

		result += left.top();
	}

	std::cout << "Result: " << result % 10000 << std::endl;
}