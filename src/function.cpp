#include <iostream>
#include <memory>
#include <tuple>
#include <function_traits.h>

template<typename> class function;

template<typename R, typename... Args>
class function<R(Args...)> {
	struct function_interface {
		virtual R operator()(Args...) = 0;
	};

	std::shared_ptr<function_interface> functor;

	template<typename T>
	class type_eraser : public function_interface {
		T object;
	public:
		type_eraser(T obj):object(obj) {

		}

		virtual R operator()(Args... args) {
			return object(args...);
		}
	};

public:
	static const size_t nargs = sizeof...(Args);
	typedef R result_type;

	template<size_t i>
	struct arg {
		typedef typename std::tuple_element<i, std::tuple<Args...>>::type type;
	};

	template<typename T>
	function(T obj):functor(std::make_shared<type_eraser<T>>(obj)) {

	}

	R operator()(Args... args) {
		return (*functor)(args...);
	}
};

class add_functor {
public:
	int operator()(int a, int b) {
		return a+b;
	}
};

int add(int a, int b) {
	return a+b;
}

template<typename T>
using Type = typename T::type;

template<typename T, size_t N>
using Arg = Type<typename T::template arg<N>>;

template<typename F>
void check_function_traits(F) {
	using Traits = function_traits<F>;
    static_assert(Traits::arity == 2,"Invalid arity");
    static_assert(std::is_same<typename Traits::return_type,int>::value,"Invalid return type");
    static_assert(std::is_same<typename Traits::template arg<0>::type,int>::value,"Invalid first argument type");
    static_assert(std::is_same<typename Traits::template arg<1>::type,int>::value,"Invalid second argument type");

    static_assert(std::is_same<Arg<Traits,0>,int>::value,"Invalid first argument type");
    static_assert(std::is_same<Arg<Traits,1>,int>::value,"Invalid second argument type");
}

int main() {
	function<int(int,int)> x = [](int a, int b) {
		return a + b;
	};
	check_function_traits(x);

	function<int(int,int)> y = add_functor();
	check_function_traits(y);

	function<int(int,int)> z = add;
	check_function_traits(z);

	int a,b;
	std::cout << "Input two integers: ";
	std::cin >> a >> b;

	std::cout << "Test lambda-based function: " << x(a,b) << std::endl;
	std::cout << "Test functor-based function:  " << y(a,b) << std::endl;
	std::cout << "Test function pointer based function: " << z(a,b) << std::endl;

	return 0;
}