#include <stdio.h>
#include <stdint.h>

struct A
{
	uint32_t a;
	uint32_t b;
};

struct B {
	struct A;
	uint32_t c;
};

int main() {
	struct B b = {1, 2, 3};

	printf("b.a[%u] b.b[%u] b.c[%u]\n",b.a,b.b,b.c);

	return 0;
}