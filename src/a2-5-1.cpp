#include <iostream>
#include <iomanip>
#include <iterator>
#include <vector>
#include <unordered_map>
#include <limits>
#include <cstdint>
#include <util.h>

class Point
{
	float x;
	float y;
public:
	inline std::istream& read(std::istream &is) {
		return is >> x >> y;
	}

	inline std::ostream& write(std::ostream &os) const {
		return os << "[" << x << "," << y << "]";
	}	

	inline float distance_to(const Point &other) const {
		return hypot(x - other.x, y - other.y);
	}	
};

inline std::istream& operator>>(std::istream &is, Point &point) {
	return point.read(is);
}

inline std::ostream& operator<<(std::ostream &os, const Point &point) {
	return point.write(os);
}

std::vector<Point> read_points(std::istream &is) {
	size_t point_count;
	is >> point_count;

	std::vector<Point> points;
	points.reserve(point_count);

	std::istream_iterator<Point> begin(is), end;
	points.assign(begin, end);

	return points;
}

typedef std::vector<float> PathCostVector;
typedef std::unordered_map<uint32_t, PathCostVector> VertexSetToSolutionMap;

class VertexSet {
	uint32_t set;
public:
	VertexSet(uint32_t s):set(s) {
		
	}

	static inline VertexSet fromVertexCount(size_t vertex_number) {
		uint32_t set = 0;
		for(size_t i=0;i<vertex_number;i++) {
			set |= (1 << i);
		}
		return set;
	}

	inline bool empty() const {
		return set == 0;
	}

	inline size_t ffs() const {
		return __builtin_ffs(set);
	}

	inline bool vertex_present(size_t vertex_index) const {
		return set & (1 << vertex_index);
	}

	inline VertexSet exclude_vertex(size_t vertex_index) const {
		return set & (~(1 << (vertex_index-1)));
	}

	inline VertexSet next() const {
		//Gosper's hack
		int c = set & -set;
    	int r = set + c;
    	return (((r^set) >> 2) / c) | r;
	}

	inline operator uint32_t() const {
		return set;
	}

	inline bool operator<(const VertexSet &other) const {
		return set < other.set;
	}

	inline std::ostream& write(std::ostream &os) const {
		char s[33];
		return os << std::setw(5) << util::itoa(set, s, 2);
	}
};

inline std::ostream& operator<<(std::ostream &os, const VertexSet &set) {
	return set.write(os);
}

float shortest_valid_path_length(const std::vector<float> &distance, const VertexSetToSolutionMap::value_type &solution) {
	auto cost = solution.second.begin();
	VertexSet set(solution.first);

	if(set.empty()) {
		return distance[0] + *cost;
	}

	float min_distance = std::numeric_limits<float>::infinity();
	auto s = set;
	while(int i = s.ffs()) {
		float full_distance = distance[i] + *cost++;
		if(full_distance < min_distance) {
			min_distance = full_distance;
		}
		s = s.exclude_vertex(i);
	}

	return min_distance;
}

std::vector<std::vector<float>> precalculate_distance(const std::vector<Point> &points) {
	std::vector<std::vector<float>> distance(points.size(), std::vector<float>(points.size()));
	for(size_t i=0;i<points.size();i++) {
		for(size_t j=0;j<points.size();j++) {
			distance[i][j] = points[i].distance_to(points[j]);
		}
	}
	return distance;
}

float tsp(const std::vector<std::vector<float>> &distance) {
	const size_t size = distance.size();
	const VertexSet set_limit = (1 << (size - 1));

	VertexSetToSolutionMap prev,next;
	prev[0] = { 0 };

	for(size_t n=2;n<=size;n++) {
		auto set = VertexSet::fromVertexCount(n-1);
		while(set < set_limit) {
			PathCostVector path_cost(n-1);
			auto cost = path_cost.begin();
			auto s = set;
			while(int i = s.ffs()) {
				*cost++ = shortest_valid_path_length(distance[i], *prev.find(set.exclude_vertex(i)));
				s = s.exclude_vertex(i);
			}
			next[set] = std::move(path_cost);

			set = set.next();
		}

		prev.clear();
		std::swap(prev,next);
		//next.clear();
	}

	return shortest_valid_path_length(distance[0], *prev.begin());
}

int main(int argc, char **argv) {
	auto points = read_points(std::cin);
	auto distance = precalculate_distance(points);

	std::cout << "Shortest TSP: " << tsp(distance) << std::endl;

	return 0;
}
