#include <iostream>
#include <cstdint>
#include <graph.h>
#include <weighed_edge.h>
#include <prim_mst.h>


int main(int argc, char **argv) {
	auto graph = WeighedDigraph::fromFormat213(std::cin);

	PrimMST mst(graph, 0);

	long int total_weight = 0;
	for(auto edge: mst.spanning_tree()) {
		std::cout << edge << std::endl;
		total_weight += edge.w();
	}

	std::cout << "Total spanning tree weight: " << total_weight << std::endl;

	return 0;
}