#include <iostream>
#include <fstream>
#include <iomanip>
#include <iterator>
#include <vector>
#include <algorithm>
#include <memory>
#include <chrono>
#include <functional>
#include <cstdint>
#include <util.h>

template<class T>
uint64_t merge(T * a, const size_t a_len, T * b, const size_t b_len, T * dst) {
	T *a_end = a + a_len;
	T *b_end = b + b_len;

	uint64_t inversions = 0;
	while(a < a_end && b < b_end) {
		if(*a < *b) {
			*dst++ = *a++;
		} else {
			inversions += a_end - a;
			*dst++ = *b++;
		}
	}
	while(a < a_end) *dst++ = *a++;
	while(b < b_end) *dst++ = *b++;

	return inversions;
}

template<class T>
uint64_t insertion_sort(T * const a, const size_t len) {
	uint64_t inversions = 0;
	for(size_t i=0;i<len;i++) {
		for(T *p=a+i;p>a;p--) {
			if(*p < *(p-1)) {
				std::swap(*p, *(p-1));
				++inversions;
			} else break;
		}
	}
	return inversions;
}

template<class T>
uint64_t mergesort_bottom_up(T * src, const size_t len) {
	T *result = src;
	std::unique_ptr<T> extra(new T[len]);
	T *dst = extra.get();

	uint64_t inversions = 0;

	const size_t cutoff = 32;

	for(size_t i=0;i+cutoff<=len;i+=cutoff) {
		inversions += insertion_sort(src+i,cutoff);
	}
	inversions += insertion_sort(src+len-(len % cutoff),len % cutoff);

	for(size_t k=cutoff;k<len;k*=2) {
		for(size_t i=0;i+2*k<=len;i+=2*k) {
			//std::cout << "merge: " << i << "[" << k << "] + " << i+k << "[" << k << "]" << std::endl;
			inversions += merge(src+i,k,src+i+k,k,dst+i);
		}

		size_t rest = len % (2*k);		
		if(rest > k) {
			//std::cout << "merge: " << len-rest << "-" << len-rest+k-1 << " + " << len-rest+k << "-" << len - 1 << std::endl;
			inversions += merge(src+len-rest,k,src+len-rest+k,rest-k,dst+len-rest);
		} else if(rest) {
			std::copy(src+len-rest,src+len,dst+len-rest);
		}

		std::swap(src,dst);
		//for(int i=0;i<len;i++) std::cout << *(src+i) << " ";
		//std::cout << std::endl;
	}

	if(src != result) std::copy(src,src+len,result);

	return inversions;
}

template<class T>
uint64_t mergesort_top_down(T * const src, const size_t len, T * aux=0) {
	const size_t cutoff = 32;
	if(len <= cutoff) return insertion_sort(src, len);

	bool del = false;
	if(!aux) {
		aux = new T[len];
		del = true;
	}

	uint64_t left = mergesort_top_down(src, len/2, aux);
	uint64_t right = mergesort_top_down(src + len/2, len - len/2, aux + len/2);
	uint64_t between = merge(src, len/2, src+len/2, len-len/2, aux);

	std::copy(aux, aux+len, src);

	if(del) delete[] aux; 

	return left+right+between;
}

template<class T>
class PivotSelector {
public:
	virtual size_t select(T * const src, const size_t len) const = 0;
	virtual ~PivotSelector() {};
};

template<class T>
class PivotLeft : public PivotSelector<T> {
public:
	virtual size_t select(T * const src, const size_t len) const {
		return 0;
	}
	virtual ~PivotLeft() {}
};

template<class T>
class PivotRight : public PivotSelector<T> {
public:
	virtual size_t select(T * const src, const size_t len) const {
		return len-1;
	}
	virtual ~PivotRight() {}
};

template<class T>
class PivotMedian : public PivotSelector<T> {
public:
	static size_t middle(size_t len) {
		return len % 2 ? len/2 : (len/2-1);
	}

	size_t select(T * const src, const size_t len) const {
		size_t idx[] = {0,middle(len),len-1};

		std::sort(idx, idx+sizeof(idx)/sizeof(size_t), [=](size_t a, size_t b) {
			return src[a] < src[b];
		});

		return idx[middle(sizeof(idx)/sizeof(size_t))];
	}
	virtual ~PivotMedian() {}
};


template<class T>
uint64_t quicksort_pivot_left(T * const src, const size_t len) {
	if(len <= 1) return 0;

	const T pivot = *src;

	T *left = src;
	T *right = src + len;
	while(true) {
		while(*++left < pivot) if(left == src + len - 1) break;
		while(*--right > pivot) if(right == src) break;
		if(left >= right) break;
		std::swap(*left, *right);
	}
	std::swap(*src, *right);

	/*for(T *x=src;x<src+len;x++) {
		if(x==left) std::cerr << "[";
		if(x==right) std::cerr << "{";
		std::cerr << *x;
		if(x==right) std::cerr << "}";
		if(x==left) std::cerr << "]";		
		std::cerr << " ";
	}
	std::cerr << std::endl;*/

	uint64_t left_comparisons = quicksort_pivot_left(src, right-src);
	uint64_t right_comparisons = quicksort_pivot_left(right+1, (src+len)-(right+1));

	return left_comparisons + right_comparisons + len - 1;
}

template<class T>
uint64_t quicksort_algs(T * const src, const size_t len, const PivotSelector<uint64_t> &selector) {
	if(len <= 1) return 0;	

	std::swap(*src, *(src+selector.select(src,len)));
	const T pivot = *src;

	T *j = src+1;
	for(T *i=src+1;i<src+len;i++) {
		if(*i <= pivot) std::swap(*i,*j++);
 	}

 	std::swap(*src, *(j-1));

 	/*for(T *x=src;x<src+len;x++) {
		if(x==j) std::cerr << "[";
		std::cerr << *x;
		if(x==j) std::cerr << "]";		
		std::cerr << " ";
	}
	std::cerr << std::endl;*/

	uint64_t left_comparisons = quicksort_algs(src, j-src-1, selector);
	uint64_t right_comparisons = quicksort_algs(j, (src+len)-j, selector);

	return left_comparisons + right_comparisons + len - 1;
}

template<class T>
void test(const std::string &title, std::vector<T> &array, std::vector<T> &reference, std::function<void(std::vector<uint64_t>&)> callback) {
	std::vector<T> test_array(array);
	auto duration = measure([&]() {
		callback(test_array);
	});
	std::cout << std::setw(10) << title << ": " << std::setw(7) << duration.count() << " " << (test_array == reference ? "+" : "-") << std::endl;
}

int main(int argc, char **argv) {
    std::vector<uint64_t> numbers;
    numbers.reserve(10000000);
	auto read_time = measure([&]() {
        auto in = get_input_stream(argc,argv);
        std::istream_iterator<uint64_t> begin(*in),end;
		numbers.assign(begin, end);
	});
	
	std::cout << "Read time: " << read_time.count() << std::endl;
	std::cout << "Size: " << numbers.size() << std::endl;

	std::vector<uint64_t> reference(numbers);
	std::sort(reference.begin(), reference.end());

	test("Warmup", numbers, reference, [](std::vector<uint64_t> &a) {
		std::sort(a.begin(),a.end());
	});

	test("Sort", numbers, reference, [](std::vector<uint64_t> &a) {
		std::sort(a.begin(),a.end());
	});

	test("Stable", numbers, reference, [](std::vector<uint64_t> &a) {
		std::stable_sort(a.begin(),a.end());
	});

	uint64_t inversions = 0;
	test("Merge BU", numbers, reference, [&](std::vector<uint64_t> &a) {
		inversions = mergesort_bottom_up(&a[0],a.size());
	});
	std::cout << "Inversions: " << inversions << std::endl;

	test("Merge TD", numbers, reference, [&](std::vector<uint64_t> &a) {
		inversions = mergesort_top_down(&a[0],a.size());
	});
	std::cout << "Inversions: " << inversions << std::endl;

	uint64_t comparisons = 0;
	test("Quick Sedg", numbers, reference, [&](std::vector<uint64_t> &a) {
		comparisons = quicksort_pivot_left(&a[0],a.size());
	});
	std::cout << "Comparisons: " << comparisons << std::endl;

	test("Quick left", numbers, reference, [&](std::vector<uint64_t> &a) {
		const PivotSelector<uint64_t> &selector = PivotLeft<uint64_t>();
		comparisons = quicksort_algs(&a[0],a.size(),selector);
	});
	std::cout << "Comparisons: " << comparisons << std::endl;

	test("Quick right", numbers, reference, [&](std::vector<uint64_t> &a) {
		const PivotSelector<uint64_t> &selector = PivotRight<uint64_t>();
		comparisons = quicksort_algs(&a[0],a.size(),selector);
	});
	std::cout << "Comparisons: " << comparisons << std::endl;

	test("Quick md", numbers, reference, [&](std::vector<uint64_t> &a) {
		const PivotSelector<uint64_t> &selector = PivotMedian<uint64_t>();
		comparisons = quicksort_algs(&a[0],a.size(),selector);
	});
	std::cout << "Comparisons: " << comparisons << std::endl;

	return 0;
}
