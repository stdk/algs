#include <iostream>
#include <memory>
#include <unistd.h>

#define GC_DEBUG
#include <gc/gc.h>
#include <gc/gc_cpp.h>
#include <gc/gc_allocator.h>


class TestGC : virtual public gc_cleanup {
	int idx;
	int x[100];
public:
	TestGC(int i):idx(i) {
		//std::cerr << "TestGC" << std::endl;
	}

	~TestGC() {
		std::cerr << "~TestGC " << idx << std::endl;
	}
};

int main(int argc, char **argv) {
	GC_INIT();

	for(size_t i=0;i<1000;i++) {
		TestGC *test = new TestGC(i);
		//test();
		usleep(1500);
		//std::cout << GC_get_heap_size() << "/" << sizeof(TestGC)*i << std::endl;
	}
	
	return 0;	
}