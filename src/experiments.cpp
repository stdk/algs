#include <typeinfo>
#include <iostream>
#include <functional>
#include <tuple>
#include <utility>
#include <memory>
#include <function_traits.h>
#include <curry.h>

template<typename T, size_t dimension = 0> struct extent;

template<typename T, size_t dimension, size_t N>
struct extent<T[N],dimension> 
: std::integral_constant<size_t,extent<T,dimension-1>::value> {};

template<typename T, size_t N>
struct extent<T[N], 0> : std::integral_constant<size_t, N> {};


template<typename T> struct rank : std::integral_constant<size_t, 0> {};

template<typename T,size_t N>
struct rank<T[N]> : std::integral_constant<size_t,1+rank<T>::value> {};

template<typename T>
struct rank<T[]> : std::integral_constant<size_t,1+rank<T>::value> {};


template<size_t N>
struct fact : std::integral_constant<size_t, N*fact<N-1>::value> {};

template<>
struct fact<1> : std::integral_constant<size_t, 1> {};


template<size_t N>
struct fib : std::integral_constant<size_t, fib<N-1>::value + fib<N-2>::value> {};

template<>
struct fib<1> : std::integral_constant<size_t, 1> {};

template<>
struct fib<2> : std::integral_constant<size_t, 1> {};

int main() {
	
	std::cout << "Extent 2 of int[5][8][14] is " << extent<int[5][8][14],2>::value << std::endl; 
	//std::cout << "Extent of int[] is " << extent<int[]>::value << std::endl;

	std::cout << "Rank of int[1][2][3] is " << rank<int[1][2][3]>::value << std::endl;
	std::cout << "Rank of int[][1] is " << rank<int[][1]>::value << std::endl;

	std::cout << "Factorial of 50 is equal to " << fact<50>::value << std::endl;
	std::cout << "Fibonacci number 100 is " << fib<100>::value << std::endl;

	return 0;
}