#include <iostream>
#include <vector>
#include <random>
#include <algorithm>
#include <cstdint>
#include <graph.h>
#include <util.h>

uint32_t mincut(const Digraph &graph, const size_t attempts = 1000) {
	std::random_device rd;
	std::mt19937 generator(rd());
	
	uint32_t mincut = std::numeric_limits<uint32_t>::max();

	std::vector<uint32_t> nodes(graph.V());
	std::iota(nodes.begin(), nodes.end(), 0);

	for(size_t a=0;a<attempts;a++) {
		Digraph g(graph);

		std::shuffle(nodes.begin(), nodes.end(), generator);	

     	for(auto i=nodes.begin();i!=nodes.end()-2;i++) {
     		uint32_t src = *i;

     		const Digraph::Adj &adj = g.adj(src);
     		if(!adj.size()) continue;

     		std::uniform_int_distribution<> edge_index(0,adj.size()-1);

     		uint32_t dst = adj[edge_index(generator)];
     		g.contract(dst, src);
     	}

		uint32_t current_mincut = g.adj(*nodes.rbegin()).size();
		if(current_mincut < mincut) mincut = current_mincut;
	}

	return mincut;
}

int main(int argc, char **argv) {
    auto in = get_input_stream(argc, argv);
	Digraph graph(*in);
	if(!graph) return 1;

	std::cout << mincut(graph) << std::endl;

	return 0;	
}