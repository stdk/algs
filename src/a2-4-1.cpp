#include <iostream>
#include <iomanip>
#include <memory>
#include <limits>
#include <unistd.h>
#include <stdlib.h>

class Table
{
	const int MAX;
	size_t n,m;
	std::unique_ptr<int> table;

public:
	Table(std::istream &is):MAX(std::numeric_limits<int>::max()) {
		is >> n >> m;

		table = std::unique_ptr<int>(new int[n*n]);

		for(size_t i=0;i<n;i++) {
			for(size_t j=0;j<n;j++) {
				(*this)(i,j) = i==j ? 0 : MAX;
			}
		}

		size_t src,dst,cost;
		while(is >> src >> dst >> cost) {
			(*this)(src-1,dst-1) = cost;
		}
	}

	inline size_t V() const {
		return n;
	}

	int cost_through(int from, int k, int to) {
		int a = (*this)(from, k);
		int b = (*this)(k, to);
		if(a == MAX || b == MAX) return MAX;
		return a + b;
	}

	int shortest_path() const {
		int shortest = MAX;
		for(size_t i=0;i<n;i++) {
			for(size_t j=0;j<n;j++) {
				int candidate = (*this)(i,j);
				if(candidate < shortest) {
					shortest = candidate;
				}
			}
		}
		return shortest;
	}

	bool has_negative_cycle() const {
		for(size_t i=0;i<n;i++) {
			if((*this)(i,i) < 0) return true;
		}
		return false;
	}

	inline int& operator()(size_t a, size_t b) {
		return table.get()[a*n+b];
	}

	inline const int& operator()(size_t a, size_t b) const {
		return table.get()[a*n+b];
	}

	inline std::ostream& write(std::ostream &os) const {
		for(size_t i=0;i<n;i++) {
			for(size_t j=0;j<n;j++) {
				os << std::setw(4);
				(*this)(i,j) == MAX ? os << "inf" : os << (*this)(i,j);
			}
			os << std::endl;
		}

		return os;
	}

};

inline std::ostream& operator<<(std::ostream &os, const Table &table) {
	return table.write(os);
}

void solve(Table &table, bool verbose) {
	auto V = table.V();
	for(size_t k=0;k<V;k++) {
		for(size_t i=0;i<V;i++) {
			for(size_t j=0;j<V;j++) {
				table(i,j) = std::min(table(i,j), table.cost_through(i,k,j));
			}
		}
		if(verbose) std::cout << table << std::endl;
	}
}

std::tuple<bool> parse_args(int argc, char **argv) {
	bool verbose = false;

	int opt;
	while ((opt = getopt(argc, argv, "v")) != -1) {
		switch (opt) {
		case 'v':
			verbose = true;
			break;
		default:
			std::cout << "Usage: " << argv[0] << " [-v]" << std::endl;
			std::cout << "-v   -   verbose mode" << std::endl;
			exit(EXIT_FAILURE);
		}
	}

	return std::make_tuple(verbose);
}

int main(int argc, char **argv) {
	auto opts = parse_args(argc, argv);
	bool verbose = std::get<0>(opts);

	Table table(std::cin);

	if(verbose) std::cout << table << std::endl;

	solve(table, verbose);

	if(table.has_negative_cycle()) {
		std::cout << "This graph has negative cycle." << std::endl;		
	} else {
		std::cout << "Shortest edge: " << table.shortest_path() << std::endl;		
	}

	return 0;
}