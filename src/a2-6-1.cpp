#include <iostream>
#include <cstdint>
#include <kosaraju_sharir_scc.h>
#include <util.h>
#include <unistd.h>

class Index {
	int size;
public:
	Index(int s):size(s) {

	}

	inline int to(int i) {
		return i > 0 ? i-1 : size+i;
	}

	inline int from(int i) {
		return i < size/2 ? i+1 : i - size;
	}
};

std::tuple<size_t, std::vector<std::pair<int,int>>> load_equation_data(std::istream &is) {
	uint32_t vars;
	is >> vars;	

	std::vector<std::pair<int,int>> equations;
	equations.reserve(vars);

	int x,y;
	while(is >> x >> y) {
		equations.emplace_back(std::make_pair(x,y));
	}

	return std::make_tuple(vars,equations);
}

Digraph reduce_to_implication_graph(size_t vars, const std::vector<std::pair<int,int>> &equations) {
	Digraph digraph(vars*2);
	Index idx(vars*2);

	for(auto equation: equations) {
		digraph.add_edge(idx.to(-equation.first),idx.to(equation.second));
		digraph.add_edge(idx.to(-equation.second),idx.to(equation.first));
 	}

	return digraph;
}

bool solution_exists(const KosarajuSharirSCC &scc) {
	for(size_t a=0,b=scc.V()-1;a<b;++a,--b) {
		if(scc[a] == scc[b]) return false;
	}

	return true;
}

std::vector<bool> solve(const Digraph &graph, const KosarajuSharirSCC &scc) {
	std::vector<bool> solution(graph.V());

	Index idx(graph.V());
	for(auto v: scc) {
		if(!solution[v]) {
			int x = idx.from(v);
			solution[idx.to(-x)] = true;
		}
	}

	return solution;
}

bool satisfies(const std::vector<bool> &solution, const std::vector<std::pair<int,int>> &equations) {
	Index idx(solution.size());
	for(auto equation: equations) {
		bool a = solution[idx.to(equation.first)];
		bool b = solution[idx.to(equation.second)];
		bool result = a || b;
		if(!result) {
			std::cout << equation.first << " or " << equation.second << " = " << result << std::endl;
			return false;
		}
	}

	return true;
}

std::tuple<bool,bool,bool> parse_args(int argc, char **argv) {
	bool verbose = false;
	bool show_solution = false;
	bool check_solution = false;

	int opt;
	while ((opt = getopt(argc, argv, "vsc")) != -1) {
		switch (opt) {
		case 'v':
			verbose = true;
			break;
		case 's':
			show_solution = true;
			break;
		case 'c':
			check_solution = true;
			break;
		default:
			std::cout << "Usage: " << argv[0] << " [-v] [-s] [-c]" << std::endl;
			std::cout << "-v   -   verbose mode" << std::endl;
			std::cout << "-s   -   show solution" << std::endl;
			std::cout << "-c   -   check solution" << std::endl;
			exit(EXIT_FAILURE);
		}
	}

	return std::make_tuple(verbose,show_solution,check_solution);
}

int main(int argc, char **argv) {
	
	auto opts = parse_args(argc,argv);
	bool verbose = std::get<0>(opts);
	bool show_solution = std::get<1>(opts);
	bool check_solution = std::get<2>(opts);

	std::tuple<size_t, std::vector<std::pair<int,int>>> equation_data;
	auto read_time = measure([&]() {
		equation_data = load_equation_data(std::cin);
	});
	
	if(verbose) std::cout << "Read time: " << read_time.count() << std::endl;

	Digraph digraph;
	auto reduction_time = measure([&]() {
		digraph = reduce_to_implication_graph(std::get<0>(equation_data), std::get<1>(equation_data));
	});
	if(verbose) std::cout << "Reduction time: " << reduction_time.count() << std::endl;

	KosarajuSharirSCC scc;
	auto scc_time = measure([&]() {
		scc = KosarajuSharirSCC(digraph);
	});
	if(verbose) std::cout << "SCC time: " << scc_time.count() << std::endl;

	bool has_solution = solution_exists(scc);
	auto solution = solve(digraph,scc);

	if(has_solution) {
		std::cout << "There is solution." << std::endl;

		if(show_solution) {
			for(size_t i=0;i<solution.size()/2;i++) {
				std::cout << "x" << i+1 << " = " << solution[i] << std::endl;
			}
		}

		if(check_solution) {
			if(!satisfies(solution, std::get<1>(equation_data))) {
				std::cout << "Incorrect solution." << std::endl;
				return 2;
			}
			std::cout << "Solution is correct." << std::endl;	
		}

		return 0;
	} else {
		std::cout << "There is no solution." << std::endl;
		return 1;
	}	
}