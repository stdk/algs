#include <iostream>
#include <iomanip>
#include <iterator>
#include <vector>
#include <tuple>
#include <cstdint>
#include <cstdlib>
#include <unistd.h>
#include <util.h>

struct Item {
	uint32_t value;
	uint32_t weight;

	inline std::istream& read(std::istream &is) {
		return is >> value >> weight;
	}

	inline std::ostream& write(std::ostream &os) const {
		return os << "[" << value << ":" << weight << "]";
	}

};

inline std::istream& operator>>(std::istream &is, Item &item) {
	return item.read(is);
}

inline std::ostream& operator<<(std::ostream &os, const Item &item) {
	return item.write(os);
}

size_t knapsack_full_table(size_t knapsack_size, const std::vector<Item> &items) {
	std::vector<std::vector<uint32_t>> table(knapsack_size+1, std::vector<uint32_t>(items.size()));

	auto solve = [&](ssize_t items, ssize_t weight_limit) -> uint32_t {
		if(items < 0 || weight_limit <= 0) return 0;
		return table[weight_limit][items];
	};

	for(size_t w=1;w<=knapsack_size;w++) {
		for(size_t i=0;i<items.size();i++) {
			uint32_t a = solve((ssize_t)i-1,w);
			if(items[i].weight <= w) {
				uint32_t b = items[i].value + solve(i-1,(ssize_t)w-items[i].weight);
				table[w][i] = std::max(a,b);
			} else {
				table[w][i] = a;
			}
		}
	}

	return *table.rbegin()->rbegin();
}

size_t knapsack_constrained(size_t knapsack_size, const std::vector<Item> &items) {
	std::vector<uint32_t> prev(knapsack_size+1), next(knapsack_size+1);

	for(size_t i=0;i<items.size();i++) {
		for(size_t w=1;w<=knapsack_size;w++) {
			uint32_t a = prev[w];
			if(items[i].weight <= w) {
				uint32_t b = items[i].value + prev[(ssize_t)w-items[i].weight];
				next[w] = std::max(a,b);
			} else {
				next[w] = a;
			}
		}
		std::swap(prev,next);
	}

	return *prev.rbegin();
}

std::tuple<bool,bool> parse_args(int argc, char **argv) {
	bool full_table_mode = true;
	bool verbose = true;

	int opt;
	while ((opt = getopt(argc, argv, "cs")) != -1) {
		switch (opt) {
		case 'c':
			full_table_mode = false;
			break;
		case 's':
			verbose = false;
			break;
		default:
			std::cout << "Usage: " << argv[0] << " [-c] [-s]" << std::endl;
			std::cout << "-c   -   memory constrained mode" << std::endl;
			std::cout << "-s   -   silent mode" << std::endl;
			exit(EXIT_FAILURE);
		}
	}

	return std::make_tuple(full_table_mode, verbose);
}

int main(int argc, char **argv) {
	auto opts = parse_args(argc, argv);

	bool full_table_mode = std::get<0>(opts);
	bool verbose = std::get<1>(opts);

	size_t knapsack_size, item_count;
	std::cin >> knapsack_size >> item_count;

	if(verbose) {
		std::cout << "Knapsack size: " << knapsack_size << std::endl;
		std::cout << "Item count: " << item_count << std::endl; 
		std::cout << "Mode: " << (full_table_mode ? "full_table" : "constrained") << std::endl;
	}

	std::vector<Item> items;

	auto read_time = measure([&]() {
		items.reserve(item_count);
		std::istream_iterator<Item> begin(std::cin),end;
		items.assign(begin,end);
	});

	if(verbose) {
		std::cout << "Reading time: " << read_time.count() << std::endl;
	}

	size_t solution;
	auto solving_time = measure([&]() {
		solution = (full_table_mode ? knapsack_full_table : knapsack_constrained)(knapsack_size, items);
	});

	if(verbose) {
		std::cout << "Solving time: " << solving_time.count() << std::endl;
		std::cout << solution << std::endl;
	}

	return 0;

}