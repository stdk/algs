#include <iostream>
#include <curry.h>

int main() {
	std::cout << "Input three numbers a,b,c: ";

	int a,b,c;
	std::cin >> a >> b >> c;

	auto f = [](int x, int y, int z) {
		return x + y + z;
	};	

	std::cout << "curry(f)(a,b,c) = " << curry(f)(a,b,c) << std::endl;
	std::cout << "curry(f,a)(b,c) = " << curry(f,a)(b,c) << std::endl;
	std::cout << "curry(f,a,b)(c) = " << curry(f,a,b)(c) << std::endl;
	std::cout << "curry(f,a,b,c)() = " << curry(f)(a,b,c) << std::endl;

	std::cout << "curry(curry(f,a),b)(c) = " << curry(curry(f,a),b)(c) << std::endl;

	std::cout << "curry(f,a+0.1)(b,c+0.1) = " << curry(f,a+0.1)(b,c+0.1) << std::endl;

	return 0;
}