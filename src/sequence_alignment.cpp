#include <iostream>
#include <iomanip>
#include <algorithm>
#include <memory>
#include <functional>
#include <cstring>
#include <cstdint>
#include <cstdlib>
#include <getopt.h>

struct config {
	const char *a;
	const char *b;
	size_t a_len;
	size_t b_len;
	int32_t space_cost;
	int32_t match_cost;
	int32_t diff_cost;
	bool align;
	bool verbose;

	std::ostream& write(std::ostream &os) const {
		os << "Configuration:" << std::endl;
		os << "A len = " << strlen(a) << std::endl;
		os << "B len = " << strlen(b) << std::endl;
		os << "Space cost = " << space_cost << std::endl;
		os << "Match cost = " << match_cost << std::endl;
		os << "Diff cost = " << diff_cost << std::endl;
		return os;
	}
};

std::ostream& operator<<(std::ostream& os, const config &cfg) {
	return cfg.write(os);
}

void show_alignment(std::function<int32_t& (size_t,size_t)> subproblem, const config &cfg) {
	const size_t max_len = cfg.a_len + cfg.b_len;
	std::unique_ptr<char> a(new char[max_len+1]);
	std::unique_ptr<char> b(new char[max_len+1]);
	char *a_dst = a.get() + max_len;
	char *b_dst = b.get() + max_len;
	*a_dst-- = 0;
	*b_dst-- = 0;

	size_t i = cfg.a_len;
	size_t j = cfg.b_len;

	while(i && j) {
		int32_t current = subproblem(i,j);
		int32_t diff = current - subproblem(i-1,j-1);
		if((diff == cfg.match_cost && cfg.a[i-1] == cfg.b[j-1]) || diff == cfg.diff_cost) {
			*a_dst-- = cfg.a[--i];
			*b_dst-- = cfg.b[--j];
		} else if(current == subproblem(i-1,j) + cfg.space_cost) {
			*a_dst-- = cfg.a[--i];
			*b_dst-- = '-';
		} else if(current == subproblem(i,j-1) + cfg.space_cost) {
			*a_dst-- = '-';
			*b_dst-- = cfg.b[--j];
		}		
	}

	while(i) {
		*a_dst-- = cfg.a[--i];
		*b_dst-- = '-';
	}
	while(j) {
		*a_dst-- = '-';
		*b_dst-- = cfg.b[--j];
	}

	std::cout << ++a_dst << std::endl;
	std::cout << ++b_dst << std::endl;
}

int32_t solve(const config &cfg) {
	if(cfg.verbose) std::cout << cfg;

	std::unique_ptr<int32_t[]> table(new int32_t[(cfg.a_len+1)*(cfg.b_len+1)]);
	auto subproblem = [&](size_t i, size_t j) -> int32_t& {
		return table[i*(cfg.b_len+1)+j];
	};

	for(size_t i=0;i<=cfg.a_len;i++) subproblem(i,0) = cfg.space_cost*i;
	for(size_t j=0;j<=cfg.b_len;j++) subproblem(0,j) = cfg.space_cost*j;

	for(size_t i=1;i<=cfg.a_len;i++) {
		for(size_t j=1;j<=cfg.b_len;j++) {
			subproblem(i,j) = std::min({
				cfg.space_cost + subproblem(i-1,j),
				cfg.space_cost + subproblem(i,j-1),
				(cfg.a[i-1] == cfg.b[j-1] ? cfg.match_cost : cfg.diff_cost)
				+ subproblem(i-1,j-1)
			});
		}
	}

	if(cfg.verbose) {
		for(size_t i=0;i<=cfg.a_len;i++) {
			for(size_t j=0;j<=cfg.b_len;j++) {
				std::cout << std::setw(4) << subproblem(i,j);
			}
			std::cout << std::endl;
		}
	}

	if(cfg.align) show_alignment(subproblem, cfg);

	return subproblem(cfg.a_len, cfg.b_len);
}

config parse_args(int argc, char **argv) {
	config cfg = { .a = 0, .b = 0, .a_len = 0, .b_len = 0,
		           .space_cost = 2, .match_cost = 1, .diff_cost = 3,		
		           .align = false, .verbose = false	};

	auto show_usage = [&]() {
		std::cout << "Usage: " << argv[0] << " [OPTIONS] <str1> <str2>\n";
		std::cout << "Options:\n";
		std::cout << "Mandatory arguments to long options are mandatory for short options too.\n";
		std::cout << "-v, --verbose                    show dynamic programming table" << std::endl;
		std::cout << "-a, --align                      show aligned string" << std::endl;
		std::cout << "-s, --space-cost=SPACE_COST      set space cost to given integer" << std::endl;
		std::cout << "-m, --match-cost=MATCH_COST      set match cost to given integer" << std::endl;
		std::cout << "-d, --diff-cost=DIFF_COST        set diff cost to given integer" << std::endl;
	};

	struct option opts[] = {
		{"verbose"    ,  no_argument      , 0, 'v' },
		{"align"      ,  no_argument      , 0, 'a' },
		{"space-cost" ,  required_argument, 0, 's' },
		{"match-cost" ,  required_argument, 0, 'm' },
		{"diff-cost"  ,  required_argument, 0, 'd' },
		{            0,                  0, 0,   0 }
	};

	char *endptr;
	int c;
	while ((c = getopt_long(argc, argv, "vas:m:d:", opts, 0)) != -1) {
		switch (c) {
		case 'v':
			cfg.verbose = true;
			break;
		case 'a':
			cfg.align = true;
			break;
		case 's':
			cfg.space_cost = strtol(optarg,&endptr,10);
			if(endptr == optarg) {
				std::cout << "space cost should be integer instead of \"" << optarg << "\"" << std::endl;
				exit(EXIT_FAILURE);
			}
			break;
		case 'm':
			cfg.match_cost = strtol(optarg,&endptr,10);
			if(endptr == optarg) {
				std::cout << "match cost should be integer instead of \"" << optarg << "\"" << std::endl;
				exit(EXIT_FAILURE);
			}
			break;
			break;
		case 'd':
			cfg.diff_cost = strtol(optarg,&endptr,10);
			if(endptr == optarg) {
				std::cout << "diff cost should be integer instead of \"" << optarg << "\"" << std::endl;
				exit(EXIT_FAILURE);
			}
			break;
			break;
		case '?':
			std::cout << "?" << std::endl;
			break;
		default:
			std::cout << std::endl;
			show_usage();
			exit(EXIT_FAILURE);
		}
	}

	if(argc - optind < 2) {
		show_usage();
		exit(EXIT_FAILURE);
	}

	cfg.a = argv[optind];
	cfg.b = argv[optind+1];

	cfg.a_len = strlen(cfg.a);
	cfg.b_len = strlen(cfg.b);

	return cfg;
}

int main(int argc, char **argv) {
	auto opts = parse_args(argc, argv);
	std::cout << "Minimal cost: " << solve(opts) << std::endl;	
}