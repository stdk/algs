#include <iostream>
#include <vector>
#include <chrono>
#include <algorithm>
#include <sstream>
#include <graph.h>
#include <kosaraju_sharir_scc.h>
#include <util.h>

int main(int argc, char **argv) {
	Digraph graph; 

	auto duration1 = measure([&]() {
        auto in = get_input_stream(argc, argv);
		graph = Digraph(*in);
	});
	std::cout << "Graph[" << graph.V() << "] loaded in: " 
	          << duration1.count() << "ms" << std::endl;

	std::vector<size_t> result(5);

	auto duration2 = measure([&]() {
		KosarajuSharirSCC scc(graph);	

		std::vector<size_t> stats = scc.stats();
		std::sort(stats.begin(),stats.end());
		std::copy(stats.rbegin(),stats.rbegin()+std::min(stats.size(),(size_t)5),result.begin());
	});
	std::cout << "Kosaraju-Sharir algorithm took: " 
	          << duration2.count() << "ms" << std::endl;

	
	std::cout << join(",", result.begin(), result.end()) << std::endl;
	return 0;
}
