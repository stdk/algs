#include <iostream>
#include <iterator>
#include <vector>
#include <algorithm>
#include <union_find.h>

struct Edge 
{
	uint32_t node1;
	uint32_t node2;
	uint32_t cost;

	inline std::istream& read(std::istream &is) {
		is >> node1 >> node2 >> cost;
		--node1;
		--node2;
		return is;
	}

	inline std::ostream& write(std::ostream &os) const {
		return os << "[" << node1 << "-" << node2 << "]:" << cost;
	}

	bool operator<(const Edge &other) const {
		return cost < other.cost;
	}
};

inline std::istream& operator>>(std::istream &is, Edge &edge) {
	return edge.read(is);
}

inline std::ostream& operator<<(std::ostream &os, const Edge &edge) {
	return edge.write(os);
}

uint32_t clustering(size_t node_count, std::vector<Edge> &edges, size_t cluster_count) {
	std::stable_sort(edges.begin(), edges.end());

	UnionFind uf(node_count);
	size_t groups_left = node_count;

	for(auto edge=edges.begin(); edge!= edges.end();++edge) {
		std::cout << *edge;
		if(uf.connected(edge->node1, edge->node2)) {
			std::cout << " already connected" << std::endl;	
		} else {
			if(groups_left > cluster_count) {
				uf.unite(edge->node1, edge->node2);
				std::cout << " contracted" << std::endl;
				groups_left -= 1;
			} else {
				std::cout << " completed" << std::endl;
				return edge->cost;
			}
		}		
	}

	return -1;
}

int main(int argc, char **argv) {

	uint32_t node_count;
	if(!(std::cin >> node_count)) {
		std::cout << "Incorrect file format!" << std::endl;
		return 1;
	}

	std::vector<Edge> edges;

	std::istream_iterator<Edge> input(std::cin);
	edges.assign(input, std::istream_iterator<Edge>());

	std::cout << "Spacing:" << clustering(node_count, edges, 4) << std::endl;

	return 0;
}