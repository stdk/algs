#include <iostream>
#include <iomanip>
#include <vector>
#include <memory>
#include <utility>
#include <type_traits>
#include <cctype>

struct Weapon {
	bool can_attack() const { return true; }
	bool can_defend() const { return true; }
};

struct Armor {
	bool can_defend() const { return true; }
};

struct ShieldScroll {
	bool can_defend() const { return true; }	
};

struct FireScroll {
	bool can_attack() const { return true; }
};

struct StoneSkinPotion {
	bool can_defend() const { return true; }
};

struct PoisonPotion {
	bool can_attack() const { return true; }
};

struct Flower {
	
};

template<typename T>
using Type = typename T::type;

template<typename Condition, typename T = void> 
using EnableIf = Type<std::enable_if<Condition::value, T>>;

template<typename Condition, typename T = void>
using EnableIfNot = Type<std::enable_if<Condition::value, T>>;

class Object {
	struct ObjectConcept {   
		virtual ~ObjectConcept() {}
		virtual bool can_attack() const = 0;
		virtual bool can_defend() const = 0;
		virtual std::string name() const = 0;
	};

	struct concept {
		template<typename A, typename = bool>
		struct attack: std::false_type {
			static inline bool get(const A &object) {
				return false;
			}
		};

		template<typename A>
		struct attack<A, decltype(std::declval<A>().can_attack())>: std::true_type {
			static inline bool get(const A &object) {
				return object.can_attack();
			}
		};

		template<typename A, typename = bool>
		struct defend: std::false_type {
			static inline bool get(const A &object) {
				return false;
			}
		};

		template<typename A>
		struct defend<A, decltype(std::declval<A>().can_defend())>: std::true_type {
			static inline bool get(const A &object) {
				return object.can_defend();
			}
		};
	};

	template<typename T> class ObjectModel : public ObjectConcept {
		T object;

	public:
		ObjectModel( const T& t ) : object( t ) {}
		virtual ~ObjectModel() {}
       
		enum { attack_concept = concept::attack<T>::value };

		virtual bool can_attack() const {
			return concept::attack<T>::get(object);
		}

		virtual bool can_defend() const {
			return concept::defend<T>::get(object);
		}

		virtual std::string name() const { 
			const char* name = typeid(object).name();
			while(std::isdigit(*name)) name++;
			return name;
		}
		
	};

	std::shared_ptr<ObjectConcept> object;

	public:
		template< typename T > Object( const T& obj ) :
		object( new ObjectModel<T>( obj ) ) {}

		std::string name() const {
			return object->name();
		}

		bool can_attack() const {
			return object->can_attack();
		}

		bool can_defend() const {
			return object->can_defend();
		}
};

int main() {
	std::vector<Object> backpack = { 
		Weapon(), 
		Armor(), 
		ShieldScroll(),
		FireScroll(),
		StoneSkinPotion(), 
		PoisonPotion(),
		Flower()
	};
	
	std::cout << std::setw(15) << "Item" 
	          << std::setw(8) << "Attack" 
	          << std::setw(8) << "Defend" << std::endl;
	for(auto item: backpack) {
		std::cout << std::setw(15) << item.name() 
		          << std::setw(8) << item.can_attack() 
		          << std::setw(8) << item.can_defend() << std::endl;
	}
}