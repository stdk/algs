#include <string>
#include <iostream>
#include <iterator>
#include <sstream>
#include <tuple>
#include <unordered_map>
#include <algorithm>
#include <cstdint>
#include <union_find.h>
#include <util.h>

class Point
{
	uint32_t pos;

public:
	Point(uint32_t p):pos(p) {

	}

	Point(const std::string &coords, size_t size):pos(0) {
		std::istringstream iss(coords);
		//std::cout << "Loading: " << coords << std::endl;

		for(size_t i=0;i<size;i++) {
			uint32_t current_bit;
			iss >> current_bit;
			pos += (current_bit << i);
		}
	}

	inline uint32_t get_pos() const {
		return pos;
	}

	inline std::ostream& write(std::ostream &os) const {
		char s[33];
		return os << "[" << util::itoa(pos, s, 2) << "]";
	}

	inline bool operator==(const Point &other) const {
		return pos == other.pos;
	}

	inline bool operator<(const Point &other) const {
		return pos < other.pos;
	}

	inline Point operator^(const Point &other) const {
		return Point(pos ^ other.pos);
	}

	size_t distance(const Point &other) const {
		return __builtin_popcount(pos ^ other.pos);
	}
};

inline std::ostream& operator<<(std::ostream &os, const Point &point) {
	return point.write(os);
}

namespace std
{
    template<>
    struct hash<Point>
    {
        typedef Point argument_type;
        typedef std::size_t value_type;
 
        value_type operator()(argument_type const &point) const
        {
        	return std::hash<uint32_t>()(point.get_pos());
        }
    };
}

std::tuple<std::unordered_map<Point, uint32_t>, size_t> load_points(std::istream &is) {
	std::string line;

	size_t point_count, coord_size;
	if(!std::getline(is,line)) {
		std::cout << "No data." << std::endl;
		return std::make_tuple(std::unordered_map<Point, uint32_t>(), 0);
	}

	std::istringstream iss(line);
	if(!(iss >> point_count >> coord_size)) {
		std::cout << "Incorrect input data format." << std::endl;
		return std::make_tuple(std::unordered_map<Point, uint32_t>(), 0);
	}

	std::unordered_map<Point, uint32_t> points(point_count*5);
	uint32_t index = 0;

	while(std::getline(is,line)) {
		auto insertion = points.insert(std::make_pair(Point(line, coord_size),index));
		if(insertion.second) index++;
	}

	return std::make_tuple(points, coord_size);
}

std::vector<Point> generate_2bit_differences(size_t coord_size) {
	std::vector<Point> points;

	points.emplace_back(0);

	for(size_t i=0;i<coord_size;i++) {
		uint32_t x = 1 << i;
		points.emplace_back(x);
		for(size_t j=i+1;j<coord_size;j++) {
			points.emplace_back(x | (1 << j));
		}
	}

	return points;
}

int analyze(const std::unordered_map<Point, uint32_t> &points, size_t coord_size, size_t spacing_lower_bound) {
	UnionFind uf(points.size());

	auto diffs = generate_2bit_differences(coord_size);

	int groups_count = points.size();

	for(auto pair: points) {
		for(auto diff: diffs) {
			auto index = points.find(pair.first ^ diff);
			if(index != points.end() && uf.unite(pair.second, index->second)) {
				--groups_count;
			}
		}
	}

	return groups_count;
}

int main() {

	std::tuple<std::unordered_map<Point, uint32_t>, size_t> input;

	auto read_time = measure([&]() {
		input = load_points(std::cin);			
	});
		
	auto points = std::get<0>(input);
	auto coord_size = std::get<1>(input);

	std::cout << "Read time: " << read_time.count() << std::endl;

	int clusters;
	auto process_time = measure([&]() {
		clusters = analyze(points, coord_size, 3);
	});	

	std::cout << "Process time: " << process_time.count() << std::endl;

	std::cout << "Maximum possible clusters: " << clusters << std::endl;	

	return 0;
}
