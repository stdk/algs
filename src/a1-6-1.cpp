#include <iostream>
#include <iterator>
#include <functional>
#include <vector>
#include <chrono>
#include <unordered_set>
#include <set>
#include <cstdint>


std::chrono::milliseconds measure(std::function<void()> callback) {
	auto start = std::chrono::high_resolution_clock::now();
	callback();
	auto end = std::chrono::high_resolution_clock::now();
	return std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
}

int main() {
	std::vector<int64_t> numbers;
	numbers.reserve(1000000);

	std::set<int64_t> set;
	auto read_time = measure([&]() {
		std::istream_iterator<int64_t> input(std::cin), end;
		while(input != end) {
			set.insert(*input);
			numbers.push_back(*input++);
		}
	});

	std::cout << "Read time: " << read_time.count() << std::endl;
	std::cout << "Vector size: " << numbers.size() << std::endl;
	std::cout << "Set size: " << set.size() << std::endl;

	
	std::vector<bool> sums(20000);
	size_t count = 0;

	for(auto n: numbers) {
		auto lower = set.lower_bound(-10000 - n);
		auto upper = set.upper_bound(10000 - n);
		//std::cout << "N: " << n << " [" << *lower << " ; " << *upper << "]" << std::endl;
		for(;lower!=upper;++lower) {
			if(n != *lower && !sums[n + *lower + 10000]) {
				sums[n + *lower + 10000] = true;
				++count;
				//std::cout << n + *lower << std::endl;
			}
		}
	}
	
	// for(int64_t i=0;i<sums.size();i++) {
	// 	if(sums[i]) std::cout << i-10000 << " ";
	// }
	// std::cout << std::endl;

	std::cout << "Count: " << count << std::endl;

	return 0;
}