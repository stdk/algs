#include <iostream>
#include <iomanip>
#include <iterator>
#include <vector>
#include <limits>

std::vector<std::vector<float>> dynamic_table(const std::vector<float> &probability) {
	const size_t size = probability.size();

	std::vector<std::vector<float>> table(size, std::vector<float>(size));

	for(size_t i=0;i<size;i++) {
		table[i][i] = probability[i];
	}

	for(size_t distance=1;distance<size;distance++) {
		for(size_t begin=0;begin<size-distance;begin++) {
			size_t end = begin + distance;
			
			float minimum = std::numeric_limits<float>::infinity();
			for(size_t root=begin;root<=end;root++) {
				float candidate = probability[root];
				for(size_t i=begin;i<=end;i++) {
					if(i!=root) candidate += probability[i];
				}

				if(root > begin) candidate += table[begin][root-1];
				if(root < end)   candidate += table[root+1][end];

				if(candidate < minimum) minimum = candidate;
			}

			table[begin][end] = minimum;
		}
	}

	for(auto row: table) {
		for(auto val: row) {
			std::cout << std::setw(5) << val << " ";
		}
		std::cout << std::endl;
	}

	return table;
}

int main(int argc, char **argv) {
	std::vector<float> probability;

	std::istream_iterator<float> begin(std::cin), end;
	probability.assign(begin,end);

	auto table = dynamic_table(probability);

	return 0;
}