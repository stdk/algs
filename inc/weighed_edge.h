#ifndef WEIGHED_EDGE
#define WEIGHED_EDGE

#include <iostream>
#include <cstdint>
#include <utility>
#include <graph.h>

class WeighedEdge {
	uint32_t vertex;
	int32_t weight;
public:
	WeighedEdge():vertex(0),weight(0) {

	}

	WeighedEdge(uint32_t v, int32_t w):vertex(v), weight(w) {
		
	}

	WeighedEdge& operator=(uint32_t v) {
		vertex = v;
		return *this;
	}

	inline explicit operator uint32_t() {
		return vertex;
	}

	WeighedEdge& operator--() {
		--vertex;
		return *this;
	}

	inline uint32_t v() const {
		return vertex;		
	}

	inline int32_t w() const {
		return weight;
	}

	WeighedEdge operator+(int32_t additional_weight) const {
		WeighedEdge result(vertex, weight + additional_weight);
		return result;
	}

	bool operator<(const WeighedEdge &other) const {
		return weight < other.weight;
	}

	bool operator>(const WeighedEdge &other) const {
		return weight > other.weight;
	}

	inline std::istream& read(std::istream &is) {
		is >> vertex >> weight;
		return is;
	}

	inline std::ostream& write(std::ostream &os) const {
		return os << vertex << "[" << weight << "]";
	}
};

inline std::ostream& operator<<(std::ostream &os, const WeighedEdge &edge) {
	return edge.write(os);
}

inline std::istream& operator>>(std::istream &is, WeighedEdge &edge) {
	return edge.read(is);
}

typedef Graph<WeighedEdge> WeighedDigraph;

struct FullEdge {
	uint32_t vertex1;
	uint32_t vertex2;
	int32_t weight;
	
public:
	FullEdge(uint32_t n, WeighedEdge e)
	:vertex1(n),vertex2(e.v()),weight(e.w()) {
		
	}

	inline uint32_t v1() {
		return vertex1;
	}

	inline uint32_t v2() {
		return vertex2;
	}

	inline int32_t w() {
		return weight;
	}

	bool operator<(const FullEdge &other) const {
		return weight < other.weight;
	}

	bool operator>(const FullEdge &other) const {
		return weight > other.weight;
	}

	inline std::istream& read(std::istream &is) {
		is >> vertex1 >> vertex2 >> weight;
		return is;
	}

	inline std::ostream& write(std::ostream &os) const {
		return os << vertex1 << "-" << vertex2 << "[" << weight << "]";
	}
};

inline std::ostream& operator<<(std::ostream &os, const FullEdge &edge) {
	return edge.write(os);
}

inline std::istream& operator>>(std::istream &is, FullEdge &edge) {
	return edge.read(is);
}

#endif