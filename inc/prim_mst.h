#ifndef PRIM_MST_H
#define PRIM_MST_H

#include <vector>
#include <queue>
#include <cstdint>
#include <graph.h>
#include <weighed_edge.h>

class PrimMST {
	std::vector<bool> marked;
	std::vector<FullEdge> tree;
	std::priority_queue<FullEdge, std::vector<FullEdge>, 
	                    std::greater<FullEdge> > queue;
public:
	PrimMST(const WeighedDigraph &graph, uint32_t v):marked(graph.V()) {

		marked[v] = true;
		for(auto edge: graph.adj(v)) {
			queue.push(FullEdge(v,edge));
		}

		while(!queue.empty()) {
			auto edge = queue.top();
			queue.pop();

			if(!marked[edge.v2()]) {
				marked[edge.v2()] = true;
				tree.push_back(edge);

				for(auto next_edge: graph.adj(edge.v2())) {
					queue.push(FullEdge(edge.v2(), next_edge));
				}
			}
		}
	}

	const std::vector<FullEdge>& spanning_tree() {
		return tree;
	}
};

#endif