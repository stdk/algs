#ifndef INDEX_SEQUENCE_H
#define INDEX_SEQUENCE_H

#include <cstddef>
#include <tuple>

/* index_sequence template provides a way to generate a compile-time list
   of unsigned integers as an arguments of sequence class template */
template<size_t... N>
struct sequence {};

template<size_t N, size_t... Args>
struct index_sequence : index_sequence<N-1,N-1,Args...> {};

template<size_t... Args>
struct index_sequence<0,Args...> {
	using type = sequence<Args...>;
};

/* tuple_concat allows us to concatenate type lists 
   of any amount of tuples into type list of a single tuple */
template<typename...> struct tuple_concat;

template<typename... TupleArgs, typename... RestOfTuples>
struct tuple_concat<std::tuple<TupleArgs...>,RestOfTuples...> {
	using type = typename tuple_concat<std::tuple<TupleArgs...>, typename tuple_concat<RestOfTuples...>::type>::type;
};

template<typename... A, typename... B>
struct tuple_concat<std::tuple<A...>,std::tuple<B...>> {
	using type = std::tuple<A...,B...>;
};

template<typename... A>
struct tuple_concat<std::tuple<A...>> {
	using type = std::tuple<A...>;
};

/* match template provides facilities for matching of two type lists 
   to detect their common prefix and suffixes of each participating type list */
template<typename X, typename Y> struct match;

template<typename... A, typename... B>
struct match<std::tuple<A...>,std::tuple<B...>> {
	using common = std::tuple<>;
	using end_a = std::tuple<A...>;
	using end_b = std::tuple<B...>;
};

template<typename Common, typename... A, typename... B>
class match<std::tuple<Common,A...>,std::tuple<Common,B...>> {
	using next = match<std::tuple<A...>,std::tuple<B...>>;
public:
	using common = typename tuple_concat<std::tuple<Common>, typename next::common>::type;
	using end_a = typename next::end_a;
	using end_b = typename next::end_b;
};


/* divide can be used to split a given type list into prefix 
   with a given length and suffix with the rest of type list */
template<size_t, typename...> struct divide;

template<size_t N, typename T, typename... Args>
struct divide<N,T,Args...> {
	using prefix = typename tuple_concat<std::tuple<T>,typename divide<N-1,Args...>::prefix>::type;
	using suffix = typename divide<N-1,Args...>::suffix;
};

template<size_t N>
struct divide<N> {
	using prefix = std::tuple<>;
	using suffix = std::tuple<>;
};

template<typename T, typename... Args>
struct divide<0,T,Args...> {
	using prefix = std::tuple<>;
	using suffix = std::tuple<T,Args...>;
};

#endif //INDEX_SEQUENCE_H