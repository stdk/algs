#ifndef KOSARAJU_SHARIR_SCC_H
#define KOSARAJU_SHARIR_SCC_H

#include <vector>
#include <set>
#include <graph.h>
#include <depth_first_order.h>

class KosarajuSharirSCC {
	std::vector<bool> marked;
	std::vector<uint32_t> id;
	std::vector<uint32_t> order;
	std::vector<size_t> component_size;

	void dfs(const Digraph &graph, uint32_t v) {
		marked[v] = true;
		id[v] = component_size.size()-1;
		order.push_back(v);
		*component_size.rbegin() += 1;

		for(auto w: graph.adj(v)) {
			if(!marked[w]) dfs(graph,w);
		}

	}
public:
	KosarajuSharirSCC() {

	}

	KosarajuSharirSCC(const Digraph &graph):marked(graph.V()),id(graph.V())
	{
		order.reserve(graph.V());

		auto post = DepthFirstOrder(graph.reversed()).post();

		for(auto i=post.rbegin();i!=post.rend();i++) {
			if(!marked[*i]) {
				component_size.push_back(0);
				dfs(graph,*i);
			}
		}
	}

	inline size_t V() const {
		return id.size();
	}

	auto begin() const -> decltype(order.rbegin()) {
		return order.rbegin();
	}

	auto end() const -> decltype(order.rend()) {
		return order.rend();
	}

	inline bool strongly_connected(uint32_t v, uint32_t w) const {
		return id[v] == id[w];
	}

	inline uint32_t operator[](size_t v) const {
		return id[v];
	}

	inline const std::vector<size_t>& stats() const {
		return component_size;
	}
};

#endif