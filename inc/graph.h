#ifndef GRAPH_H
#define GRAPH_H

#include <iostream>
#include <sstream>
#include <vector>
#include <cstdint>
#include <algorithm>
#include <type_traits>
#include <utility>
#include <cstdio>
#include <cstdint>

template<class Edge>
class Graph {
	bool ok;	
	std::vector<std::vector<Edge>> nodes;

public:
	typedef Edge edge_t;
	typedef std::vector<Edge> Adj;

	Graph():ok(false),nodes(0) {

	}

	Graph(size_t node_count):ok(true),nodes(node_count) {

	}

	Graph(std::istream &in):ok(true) {
		std::string line;
		while(std::getline(in,line)) {
			std::istringstream iss(line);

			uint32_t node_index;
			if(!(iss >> node_index)) { 
				std::cerr << "Incorrect graph format." << std::endl;
				ok = false;
				break;
			}

			--node_index;
			while(node_index >= nodes.size()) nodes.push_back(Adj());
			Adj &node = nodes[node_index];

			Edge edge;
			while(iss >> edge) {
				node.push_back(--edge);
			}
		}
	}

	static Graph fromFormat213(std::istream &in) {
		size_t node_count,edge_count;
		in >> node_count >> edge_count;		

		Graph graph(node_count);

		uint32_t node_index;
		Edge edge;
		while(in >> node_index >> edge) {
			graph.nodes[--node_index].push_back(--edge);

			uint32_t tmp = node_index;
			node_index = static_cast<uint32_t>(edge);
			edge = tmp;
			graph.nodes[node_index].push_back(edge);
		}		

		return graph;
	}

	Graph reversed() const {
		Graph reversed(V());

		for(size_t i=0;i<nodes.size();i++) {
			for(auto src: nodes[i]) {
				reversed.nodes[src].push_back(i);
			}
		}

		return reversed;
	}

	std::ostream& write(std::ostream &os) const {
		os << "graph {" << std::endl;
		for(size_t a=0;a<nodes.size();a++) {
			if(!nodes[a].empty()) {
				os << a << " -- { ";
				for(auto b: nodes[a]) os << b << " ";
				os << "}" << std::endl;
			}
		}
		return os << "}" << std::endl;
	}

	void contract(uint32_t dst_index, uint32_t src_index) {		
		Adj &dst = nodes[dst_index];
		Adj &src = nodes[src_index];

		auto new_dst_end = std::remove(dst.begin(), dst.end(), src_index);
		if(new_dst_end == dst.end()) {
			std::cerr << "Attempt to contract nonexistent edge: " << src_index << " -- " << dst_index << std::endl;
			return;
		} 

		dst.erase(new_dst_end, dst.end());
		src.erase(std::remove(src.begin(), src.end(), dst_index),src.end());

		for(auto i: src) {
			Adj &n = nodes[i];
			for(size_t k=0;k<n.size();k++) if(n[k] == src_index) n[k] = dst_index;
			dst.push_back(i);
		}

		src.clear();
	}

	void add_edge(uint32_t node, Edge edge) {
		nodes[node].push_back(edge);
	}

	inline const Adj& adj(uint32_t node) const {
		return nodes[node];
	}

	inline size_t V() const {
		return nodes.size();
	}

	inline explicit operator bool() const {
		return ok;
	}
};

template<class Edge>
std::ostream& operator<<(std::ostream& os, const Graph<Edge> &g) {
	return g.write(os);
}

typedef Graph<uint32_t> Digraph;

#endif
