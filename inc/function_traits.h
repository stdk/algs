#ifndef FUNCTION_TRAITS_H
#define FUNCTION_TRAITS_H

#include <tuple>
#include <functional>

template<class F>
struct function_traits;
 
// function pointer
template<class R, class... Args>
struct function_traits<R(*)(Args...)> : public function_traits<R(Args...)>
{};
 
template<class R, class... Args>
struct function_traits<R(Args...)>
{
    using return_type = R;

    using type = std::function<R(Args...)>;
 
    static constexpr std::size_t arity = sizeof...(Args);
 
    template <std::size_t N>
    struct arg
    {
        static_assert(N < arity, "error: invalid parameter index.");
        using type = typename std::tuple_element<N,std::tuple<Args...>>::type;
    };
};

// member function pointer
template<class C, class R, class... Args>
struct function_traits<R(C::*)(Args...)> : public function_traits<R(Args...)>
{};
 
// const member function pointer
template<class C, class R, class... Args>
struct function_traits<R(C::*)(Args...) const> : public function_traits<R(Args...)>
{};
 
// member object pointer
template<class C, class R>
struct function_traits<R(C::*)> : public function_traits<R(C&)>
{};

// functor
template<class F>
struct function_traits
{
    private:
        using call_type = function_traits<decltype(&F::operator())>;
    public:
        using return_type = typename call_type::return_type;

        using type = typename call_type::type;
 
        static constexpr std::size_t arity = call_type::arity;
 
        template <std::size_t N>
        struct arg
        {
            static_assert(N < arity, "error: invalid parameter index.");
            using type = typename call_type::template arg<N>::type;
        };
};
 
template<class F>
struct function_traits<F&> : public function_traits<F>
{};
 
template<class F>
struct function_traits<F&&> : public function_traits<F>
{};

#endif //FUNCTION_TRAITS_H