#ifndef DEPTH_FIRST_ORDER_H
#define DEPTH_FIRST_ORDER_H

#include <vector>
#include <type_traits>
#include <graph.h>

class DepthFirstOrder {
	std::vector<bool> marked;
	std::vector<size_t> preof;
	std::vector<size_t> postof;
	std::vector<uint32_t> preorder;
	std::vector<uint32_t> postorder;
	size_t pre_counter;
	size_t post_counter;

	template<class Edge, typename std::enable_if<std::is_unsigned<Edge>::value>::type* = nullptr>
	void dfs(const Graph<Edge> &graph, uint32_t v) {
		marked[v] = true;

		preorder[pre_counter] = v;
		preof[v] = pre_counter++;	
		for(auto w: graph.adj(v)) {
			if(!marked[w]) dfs(graph, w);
		}
		postorder[post_counter] = v;
		postof[v] = post_counter++;
	}

	template<class Edge, typename Edge::v = nullptr>
	void dfs(const Graph<Edge> &graph, uint32_t v) {
		marked[v] = true;

		preorder[pre_counter] = v;
		preof[v] = pre_counter++;	
		for(auto w: graph.adj(v)) {
			if(!marked[w]) dfs(graph, w.v());
		}
		postorder[post_counter] = v;
		postof[v] = post_counter++;
	}

public:
	template<class Edge>
	DepthFirstOrder(const Graph<Edge> &graph)
	:marked(graph.V()),preof(graph.V()),postof(graph.V()),
                       preorder(graph.V()),postorder(graph.V()),
                       pre_counter(0),post_counter(0) {
		for(uint32_t v=0;v<graph.V();v++) {
			if(!marked[v]) dfs(graph, v);
		}
	}

	const std::vector<uint32_t>& pre() const {
		return preorder;
	}

	uint32_t pre(uint32_t v) const {
		return preof[v];
	}

	const std::vector<uint32_t>& post() const {
		return postorder;
	}

	uint32_t post(uint32_t v) const {
		return postof[v];
	}


};

#endif