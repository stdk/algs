#ifndef UNION_FIND_H
#define UNION_FIND_H

#include <vector>
#include <cstdint>

class UnionFind 
{
	struct Item {
		uint32_t n;
		uint32_t w;

		Item():n(0),w(1) {}		
	};

	std::vector<Item> idx;

	Item* find_item(uint32_t a) {
		while(idx[a].n != a) a = idx[a].n;
		return &idx[a];	
	}

public:
	UnionFind(size_t size):idx(size) {
		for(size_t i=0;i<size;i++) {
			idx[i].n = i;
		}
	}

	uint32_t find(uint32_t a) const {
		const Item *i = &idx[a];
		while(i->n != a) {
			a = i->n;
			i = &idx[i->n];
		} 
		return i->n;
	}

	bool connected(uint32_t a, uint32_t b) const {
		return find(a) == find(b);
	}

	bool unite(uint32_t x, uint32_t y) {
		Item *a = find_item(x);
		Item *b = find_item(y);

		if(a == b) return false;

		if(a->w < b->w) {
			a->n = b->n;
			b->w += a->w;
		} else {
			b->n = a->n;
			a->w += b->w;
		}

		return true;
	}
};

#endif