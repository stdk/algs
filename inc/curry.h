#ifndef CURRY_H
#define CURRY_H

#include <functional>
#include <tuple>
#include <type_util.h>
#include <function_traits.h>

template<typename...> class currier_impl;

template<typename R, typename... BindArgs, typename... FreeArgs, size_t... Index>
class currier_impl<R,std::tuple<BindArgs...>, std::tuple<FreeArgs...>, sequence<Index...>> {
	std::function<R(BindArgs...,FreeArgs...)> function;
	std::tuple<BindArgs...> arguments;
public:
	currier_impl(decltype(function) f, BindArgs... args):function(f),arguments(args...) {}

	R operator()(FreeArgs... args) {
		return function(std::get<Index>(arguments)..., args...);
	}
};

template<typename...> struct currier;

template<typename R, typename... Args, typename... BindArgs>
struct currier<std::function<R(Args...)>, BindArgs...> {
	using index = typename index_sequence<sizeof...(BindArgs)>::type;
	using division = divide<sizeof...(BindArgs),Args...>;
	using type = currier_impl<R, typename division::prefix, typename division::suffix, index>;
};

template<typename F, typename... Args>
using Currier = typename currier<typename function_traits<F>::type, Args...>::type;

template<typename F, typename... Args>
auto curry(F function, Args... args) -> Currier<F,Args...> {
	return Currier<F,Args...>(function, args...);
}

#endif //CURRY_H