#!/usr/bin/env python
import __main__
import sys
from random import randint

if len(sys.argv) < 3:
    print 'Usage:',sys.modules['__main__'].__file__,'<filename>','<count>' 
    sys.exit(1)

filename = sys.argv[1]
count = int(sys.argv[2])

f = open(filename,'w')
for i in xrange(count):
    f.write('%i\n' % (randint(0, 2**32-1),))


